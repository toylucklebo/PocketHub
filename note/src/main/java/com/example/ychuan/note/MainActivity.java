package com.example.ychuan.note;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
            .show();
      }
    });
    initData();
    initRecyclerView();
  }

  List<String> mStringList = new ArrayList<>();

  private void initData() {
    for (int i = 0; i < 100; i++) {
      mStringList.add(i + "号烦人");
    }
  }

  private void initRecyclerView() {
    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
    LinearLayoutManager manager = new LinearLayoutManager(MainActivity.this);
    manager.setOrientation(LinearLayoutManager.VERTICAL);
    recyclerView.setLayoutManager(manager);

    recyclerView.setAdapter(new MyRecy());
  }

  class MyRecy extends RecyclerView.Adapter {
    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, null);
      return new Holder(inflate);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
      ((Holder) holder).mTv.setText(mStringList.get(position));
    }

    @Override public int getItemCount() {
      return mStringList.size();
    }
  }

  class Holder extends RecyclerView.ViewHolder {

    public final TextView mTv;

    public Holder(View itemView) {
      super(itemView);
      mTv = (TextView) itemView.findViewById(R.id.tv_item);
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
